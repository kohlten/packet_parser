package packet_parser

import (
	"errors"
	"net"
	"reflect"
	"strconv"
)

type PacketData struct {
	Action uint8
	Data   [][]byte
	Addr   *net.UDPAddr
	ReceiveTime uint64
}

type Packet struct {
	Data []byte
	Addr   *net.UDPAddr
	ReceiveTime uint64
}

const (
	// Packet Separator
	Start     = 0x7f
	End       = 0x80
	Separator = 0x81
)

const (
	parseError  = "invalid packet"
)

func IsValidPacket(packet []byte) bool {
	if len(packet) < 3 {
		return false
	}
	if packet[0] != Start {
		return false
	}
	for i := 2; i < len(packet); {
		if packet[i] != Separator && packet[i] != End {
			return false
		}
		if i >= len(packet) {
			return false
		}
		if packet[i] == End {
			return true
		}
		if packet[i] == Separator {
			i++
			for i < len(packet) && packet[i] != Separator && packet[i] != End {
				i++
			}
			if i >= len(packet) && packet[i] != End {
				return false
			}
		}
	}
	return true
}

func FormatPacket(action uint8, a ...interface{}) []byte {
	packet := make([]byte, 0)
	packet = append(packet, Start)
	packet = append(packet, byte(action))
	for _, arg := range a {
		packet = append(packet, Separator)
		switch v := arg.(type) {
		case int, int8, int16, int32, int64:
			packet = strconv.AppendInt(packet, reflect.ValueOf(v).Int(), 10)
		case uint, uint8, uint16, uint32, uint64:
			packet = strconv.AppendUint(packet, reflect.ValueOf(v).Uint(), 10)
		case float32:
			packet = strconv.AppendFloat(packet, reflect.ValueOf(v).Float(), 'f', 5, 32)
		case float64:
			packet = strconv.AppendFloat(packet, reflect.ValueOf(v).Float(), 'f', 5, 64)
		case string:
			packet = append(packet, reflect.ValueOf(v).String()...)
		case []byte:
			packet = append(packet, reflect.ValueOf(v).Bytes()...)
		}
	}
	packet = append(packet, End)
	return packet
}

func SendPacket(conn *net.UDPConn, addr *net.UDPAddr, action uint8, a ...interface{}) {
	packet := FormatPacket(action, a...)
	if addr != nil {
		_, _ = conn.WriteToUDP(packet, addr)
	} else {
		_, _ = conn.Write(packet)
	}
}

func ParsePacket(packet []byte, addr *net.UDPAddr) (*PacketData, error) {
	if !IsValidPacket(packet) {
		return nil, errors.New(parseError)
	}
	unpacked := PacketData{}
	unpacked.Action = uint8(packet[1])
	for i := 2; i < len(packet); {
		if packet[i] != Separator && packet[i] != End {
			return nil, errors.New(parseError)
		}
		if i >= len(packet) {
			return nil, errors.New(parseError)
		}
		if packet[i] == End {
			break
		}
		if packet[i] == Separator {
			i++
			start := i
			for i < len(packet) && packet[i] != Separator && packet[i] != End {
				i++
			}
			if i >= len(packet) && packet[i] != End {
				return nil, errors.New(parseError)
			}
			unpacked.Data = append(unpacked.Data, packet[start:i])
		}

	}
	unpacked.Addr = addr
	return &unpacked, nil
}
